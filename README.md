
### Google Summer Of Code 2018

**Project**: 
Efficient frequency spectrum computation over large sample sizes

**Organiztion**: Canadian Center for Computational Genomics


>Moments implements methods for demographic history and selection
 inference from genetic data, based on diffusion approximations 
 to the allele frequency spectrum. moments is based on the ∂a∂i
  open source package developed by Ryan Gutenkunst
   [http://gutengroup.mcb.arizona.edu]. We reused 
   ∂a∂i's interface but introduced a new spectrum 
   simulation engine. This new method is based on the direct
    computation of the frequency spectrum without solving the 
    PDE diffusion system. Consequently we make less approximations
     and we get rid of the frequency grids used in ∂a∂i. This approach
      is particularly efficient for multiple populations models 
      (up to 5 populations).

#### Contribution: 
In this summer, I work with Dr. Gravel and Dr. Ragsdale on further improving moments software. 
I will list my contributions to the software in the following part.

##### 1. Jackknife error checking during integration
In moments, the evolution equation for an AFS (allele frequency spectrum) is approximated
by a technique called Jackknife. Jackknife efficiently approximates larger sample AFS by
computing a linear combination of entries in smaller size AFS. In general, Jackknife works well
with large sample size. However, it may introduce error and negative elements for small sample size, 
which accumulates during integration. 
 
I implemented an Jackknife error checking module to warn users when there's large error and negative elements.
The input AFS can be arbitrary dimensional, say n dimensional ,the module would check each dimension separately
by fixing n-1 dimensions. The module is called at each time stamp during integration to ensure the error
doesn't accumulate. 

Link: [https://bitbucket.org/RuijianSZ/moments/src/jk_error_check/]

##### 2. Jackknife recovery & Grid generator
In original moments, Jackknife is implemented to project AFS of sample size n - 1, n - 2 to that of sample size
of n. However, we wants to use Jackknife to accurately recover the AFS by a small fraction of all entries. The grid should
also be optimally chosen for recovery accuracy. This work is important since it enables the software to integrate much larger AFS with little
computational cost. I extended the existing module by calculating the Jackknife coefficient based on the methods on this paper.[http://www.genetics.org/content/early/2017/05/08/genetics.117.200493]
I also did a lot of exploration on how to choose the optimal grid. I tried partitions of integers, random generator with monotone functions
and a deterministic grid by breaking down the AFS. It turns out the last method works best. 

Link: [https://github.com/RuijianSZ/google-summer-of-code/tree/master/JackknifeRecover]

##### 3. Sparse integrator for 1D AFS 
Armed with new Jackknife, we hope to integrate a large AFS by tracking only a subset of entries and recovering it by Jackknife. 

I implemented a new sparse integrator where users can specify the sparsity (number of entries to keep). A module to combine 
coefficient matrix in the linear system with Jackknife coefficient is implemented for integrating on the subset of entries.

Link: [https://bitbucket.org/RuijianSZ/moments/src/sparse_integration/]

##### 4. Fast test mode & Python 3 compatibility
Originally, moments requires python 2 environment to work properly. Due to popularity of python 3, we hope to
extend the codebase to support python 3 environment. Hence, I removed/replaced commands that doesn't work in python e
and fixed import issues. Also, the test suite built in moments takes several minutes
to go over all cases. Under some circumstances, a fast mode to verify changes is desired. I separated tests cases
by execution time. The new cases can also be easily added to fast mode. 

Link: [https://bitbucket.org/RuijianSZ/moments/src/python3/], [https://bitbucket.org/RuijianSZ/moments/src/test_case/] 

**Commit history**:

Link: [https://bitbucket.org/RuijianSZ/moments/commits/all], [https://github.com/RuijianSZ/google-summer-of-code/commits/master]